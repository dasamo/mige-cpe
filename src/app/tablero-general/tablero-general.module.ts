import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaGeneralComponent } from './components/consulta-general/consulta-general.component';



@NgModule({
  declarations: [ConsultaGeneralComponent],
  imports: [
    CommonModule
  ]
})
export class TableroGeneralModule { }
