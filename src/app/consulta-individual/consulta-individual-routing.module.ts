import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaIndividualComponent } from './components/consulta-individual/consulta-individual.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ConsultaIndividualComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultaIndividualRoutingModule { }


