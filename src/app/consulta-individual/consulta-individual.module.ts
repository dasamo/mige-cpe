import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaIndividualComponent } from './components/consulta-individual/consulta-individual.component';
import { ConsultaIndividualRoutingModule } from './consulta-individual-routing.module';
import { MaterialIuModule } from '../material-iu/material-iu.module';


@NgModule({
  declarations: [ConsultaIndividualComponent],
  imports: [
    CommonModule,
    ConsultaIndividualRoutingModule,
    MaterialIuModule,

  ]
})
export class ConsultaIndividualModule { }
