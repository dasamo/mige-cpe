import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RutasConstante } from './constantes/rutas.contante';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: RutasConstante.CONSULTA_INDIVIDUAL
  },
  {
    path: RutasConstante.CONSULTA_INDIVIDUAL,
    loadChildren: () => import('./consulta-individual/consulta-individual.module').
      then(m => m.ConsultaIndividualModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


