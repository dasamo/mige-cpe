import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DarConformidadComponent } from './dar-conformidad.component';

describe('DarConformidadComponent', () => {
  let component: DarConformidadComponent;
  let fixture: ComponentFixture<DarConformidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DarConformidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DarConformidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
