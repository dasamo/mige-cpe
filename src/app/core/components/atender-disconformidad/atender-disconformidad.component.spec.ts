import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtenderDisconformidadComponent } from './atender-disconformidad.component';

describe('AtenderDisconformidadComponent', () => {
  let component: AtenderDisconformidadComponent;
  let fixture: ComponentFixture<AtenderDisconformidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtenderDisconformidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtenderDisconformidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
