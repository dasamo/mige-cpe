import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionMotivosSubsanacionComponent } from './seleccion-motivos-subsanacion.component';

describe('SeleccionMotivosSubsanacionComponent', () => {
  let component: SeleccionMotivosSubsanacionComponent;
  let fixture: ComponentFixture<SeleccionMotivosSubsanacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionMotivosSubsanacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionMotivosSubsanacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
