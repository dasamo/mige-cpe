import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarNuevoPagoComponent } from './registrar-nuevo-pago.component';

describe('RegistrarNuevoPagoComponent', () => {
  let component: RegistrarNuevoPagoComponent;
  let fixture: ComponentFixture<RegistrarNuevoPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarNuevoPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarNuevoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
