import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEventosComprobanteComponent } from './ver-eventos-comprobante.component';

describe('VerEventosComprobanteComponent', () => {
  let component: VerEventosComprobanteComponent;
  let fixture: ComponentFixture<VerEventosComprobanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerEventosComprobanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEventosComprobanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
