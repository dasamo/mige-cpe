import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsanarComprobanteComponent } from './subsanar-comprobante.component';

describe('SubsanarComprobanteComponent', () => {
  let component: SubsanarComprobanteComponent;
  let fixture: ComponentFixture<SubsanarComprobanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsanarComprobanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsanarComprobanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
