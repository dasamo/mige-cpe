import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaMasivaComprobantesAtencionComponent } from './carga-masiva-comprobantes-atencion.component';

describe('CargaMasivaComprobantesAtencionComponent', () => {
  let component: CargaMasivaComprobantesAtencionComponent;
  let fixture: ComponentFixture<CargaMasivaComprobantesAtencionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargaMasivaComprobantesAtencionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaMasivaComprobantesAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
