import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmarSubsanacionComponent } from './confirmar-subsanacion.component';

describe('ConfirmarSubsanacionComponent', () => {
  let component: ConfirmarSubsanacionComponent;
  let fixture: ComponentFixture<ConfirmarSubsanacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmarSubsanacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmarSubsanacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
