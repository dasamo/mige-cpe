import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerConstanciaEnvioComponent } from './ver-constancia-envio.component';

describe('VerConstanciaEnvioComponent', () => {
  let component: VerConstanciaEnvioComponent;
  let fixture: ComponentFixture<VerConstanciaEnvioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerConstanciaEnvioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerConstanciaEnvioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
