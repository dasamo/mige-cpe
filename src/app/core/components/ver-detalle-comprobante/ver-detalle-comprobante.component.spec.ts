import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerDetalleComprobanteComponent } from './ver-detalle-comprobante.component';

describe('VerDetalleComprobanteComponent', () => {
  let component: VerDetalleComprobanteComponent;
  let fixture: ComponentFixture<VerDetalleComprobanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerDetalleComprobanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerDetalleComprobanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
