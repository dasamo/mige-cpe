import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DarDisconformidadComponent } from './dar-disconformidad.component';

describe('DarDisconformidadComponent', () => {
  let component: DarDisconformidadComponent;
  let fixture: ComponentFixture<DarDisconformidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DarDisconformidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DarDisconformidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
