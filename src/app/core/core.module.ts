import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DarConformidadComponent } from './components/dar-conformidad/dar-conformidad.component';
import { DarDisconformidadComponent } from './components/dar-disconformidad/dar-disconformidad.component';
import { ConfirmarSubsanacionComponent } from './components/confirmar-subsanacion/confirmar-subsanacion.component';
import { SubsanarComprobanteComponent } from './components/subsanar-comprobante/subsanar-comprobante.component';
import { SeleccionMotivosSubsanacionComponent } from './components/seleccion-motivos-subsanacion/seleccion-motivos-subsanacion.component';
import { AtenderDisconformidadComponent } from './components/atender-disconformidad/atender-disconformidad.component';
import { VerEventosComprobanteComponent } from './components/ver-eventos-comprobante/ver-eventos-comprobante.component';
import { VerDetalleComprobanteComponent } from './components/ver-detalle-comprobante/ver-detalle-comprobante.component';
import { CargaMasivaComprobantesAtencionComponent } from './components/carga-masiva-comprobantes-atencion/carga-masiva-comprobantes-atencion.component';
import { RegistrarPagosComponent } from './components/registrar-pagos/registrar-pagos.component';
import { RegistrarNuevoPagoComponent } from './components/registrar-nuevo-pago/registrar-nuevo-pago.component';
import { VerConstanciaEnvioComponent } from './components/ver-constancia-envio/ver-constancia-envio.component';
import { DetalleNotaCreditoComponent } from './components/detalle-nota-credito/detalle-nota-credito.component';



@NgModule({
  declarations: [DarConformidadComponent, DarDisconformidadComponent, ConfirmarSubsanacionComponent, SubsanarComprobanteComponent, SeleccionMotivosSubsanacionComponent, AtenderDisconformidadComponent, VerEventosComprobanteComponent, VerDetalleComprobanteComponent, CargaMasivaComprobantesAtencionComponent, RegistrarPagosComponent, RegistrarNuevoPagoComponent, VerConstanciaEnvioComponent, DetalleNotaCreditoComponent],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
